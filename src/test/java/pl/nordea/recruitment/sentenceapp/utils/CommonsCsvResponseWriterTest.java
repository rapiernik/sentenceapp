package pl.nordea.recruitment.sentenceapp.utils;

import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import pl.nordea.recruitment.sentenceapp.SentenceappApplication;
import pl.nordea.recruitment.sentenceapp.model.Sentences;
import pl.nordea.recruitment.sentenceapp.service.SentenceService;
import pl.nordea.recruitment.sentenceapp.utils.impl.CommonsCsvResponseWriter;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SentenceappApplication.class)
@WebAppConfiguration
public class CommonsCsvResponseWriterTest {

    @Autowired
    private CommonsCsvResponseWriter writer;

    @Autowired
    private SentenceService sentenceService;

    // remove @Ignore to check parsing from large file
    @Ignore
    @Test
    public void test() throws IOException {
        InputStream inputStream = new FileInputStream("src/test/resources/large.in");

        Sentences sentences = sentenceService.processFile(IOUtils.toByteArray(inputStream));

        PrintWriter printWriter = new PrintWriter("large.csv");
        writer.writeToFile(printWriter, sentences);
    }
}
