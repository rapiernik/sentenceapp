package pl.nordea.recruitment.sentenceapp.utils;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.nordea.recruitment.sentenceapp.utils.impl.StanfordAndOpenNLPTextParser;

import java.util.List;

public class StanfordAndOpenNLPTextParserTest {

    private static TextParser parser;

    @BeforeClass
    public static void init() {
        parser = new StanfordAndOpenNLPTextParser();
    }

    @Test
    public void shouldParseTextToSentences_ExclamationMark() {
        // given
        String text = "What he    shouted was shocking:  停在那儿, 你这肮脏的掠夺者! I couldn't understand a word,perhaps because Chinese \n" +
                " isn't my mother tongue. ";

        // when
        List<String> sentences = parser.splitToSentences(text);

        // then
        Assert.assertEquals(2, sentences.size());
        Assert.assertEquals("What he    shouted was shocking:  停在那儿, 你这肮脏的掠夺者!", sentences.get(0));
        Assert.assertEquals("I couldn't understand a word,perhaps because Chinese \n" +
                " isn't my mother tongue.", sentences.get(1));
    }

    @Test
    public void shouldParseTextToSentences_TitleAndName() {
        // given
        String text = "I was just standing there watching Mr. Young marching around - he \n" +
                "was    furious.";

        // when
        List<String> sentences = parser.splitToSentences(text);

        // then
        Assert.assertEquals(1, sentences.size());
        Assert.assertEquals("I was just standing there watching Mr. Young marching around - he \n" +
                "was    furious.", sentences.get(0));
    }

    @Test
    public void shouldParseTextToSentences_QuestionMark() {
        // given
        String text = "Why was he directing  his  anger at me? Little did I know    about    that.";

        // when
        List<String> sentences = parser.splitToSentences(text);

        // then
        Assert.assertEquals(2, sentences.size());
        Assert.assertEquals("Why was he directing  his  anger at me", sentences.get(0));
        Assert.assertEquals(" Little did I know    about    that.", sentences.get(1));
    }

    @Test
    public void shouldParseTextToSentences_ManyLines() {
        // given
        String text = "Nordea Markets is the \n" +
                "leading international \n" +
                "capital markets operator \n" +
                "and investment banking partner \n" +
                "in the Nordic and \n" +
                "Baltic Sea regions.";

        // when
        List<String> sentences = parser.splitToSentences(text);

        // then
        Assert.assertEquals(1, sentences.size());
        Assert.assertEquals("Nordea Markets is the \n" +
                "leading international \n" +
                "capital markets operator \n" +
                "and investment banking partner \n" +
                "in the Nordic and \n" +
                "Baltic Sea regions.", sentences.get(0));
    }

    @Test
    public void shouldParseTextToSentences_ParenthesisApostrophe() {
        // given
        String text = "    In fact - in all of the Nordics, you’d have a hard time finding a product range as strong and diversified as ours (and we can give you excellent liquidity in local currencies too).";

        // when
        List<String> sentences = parser.splitToSentences(text);

        // then
        Assert.assertEquals(1, sentences.size());
        Assert.assertEquals("In fact - in all of the Nordics, you’d have a hard time finding a product range as strong and diversified as ours (and we can give you excellent liquidity in local currencies too).", sentences.get(0));
    }

    @Test
    public void shouldParseSentenceToOrderedWords_Chinese() {
        // given
        String sentence = "What he    shouted was shocking:  停在那儿, 你这肮脏的掠夺者!";

        // when
        List<String> orderedWords = parser.splitToWordsOrdered(sentence);

        // then
        Assert.assertEquals(7, orderedWords.size());
        Assert.assertEquals("he", orderedWords.get(0));
        Assert.assertEquals("shocking", orderedWords.get(1));
        Assert.assertEquals("shouted", orderedWords.get(2));
        Assert.assertEquals("was", orderedWords.get(3));
        Assert.assertEquals("What", orderedWords.get(4));
        Assert.assertEquals("你这肮脏的掠夺者", orderedWords.get(5));
        Assert.assertEquals("停在那儿", orderedWords.get(6));
    }

    @Test
    public void shouldParseSentenceToOrderedWords_Apostrophe() {
        // given
        String sentence = "I couldn't understand a word,perhaps because Chinese isn't my mother tongue.";

        // when
        List<String> orderedWords = parser.splitToWordsOrdered(sentence);

        // then
        Assert.assertEquals(12, orderedWords.size());
        Assert.assertEquals("a", orderedWords.get(0));
        Assert.assertEquals("because", orderedWords.get(1));
        Assert.assertEquals("Chinese", orderedWords.get(2));
        Assert.assertEquals("couldn't", orderedWords.get(3));
        Assert.assertEquals("I", orderedWords.get(4));
        Assert.assertEquals("isn't", orderedWords.get(5));
        Assert.assertEquals("mother", orderedWords.get(6));
        Assert.assertEquals("my", orderedWords.get(7));
        Assert.assertEquals("perhaps", orderedWords.get(8));
        Assert.assertEquals("tongue", orderedWords.get(9));
        Assert.assertEquals("understand", orderedWords.get(10));
        Assert.assertEquals("word", orderedWords.get(11));
    }

    @Test
    public void shouldParseSentenceToOrderedWords_TitleAndName() {
        // given
        String sentence = "Mr. and Ms. Smith met Dr. Jekyll outside.";

        // when
        List<String> orderedWords = parser.splitToWordsOrdered(sentence);

        // then
        Assert.assertEquals(8, orderedWords.size());
        Assert.assertEquals("and", orderedWords.get(0));
        Assert.assertEquals("Dr.", orderedWords.get(1));
        Assert.assertEquals("Jekyll", orderedWords.get(2));
        Assert.assertEquals("met", orderedWords.get(3));
        Assert.assertEquals("Mr.", orderedWords.get(4));
        Assert.assertEquals("Ms.", orderedWords.get(5));
        Assert.assertEquals("outside", orderedWords.get(6));
        Assert.assertEquals("Smith", orderedWords.get(7));
    }

    @Test
    public void shouldParseSentenceToOrderedWords_Parenthesis() {
        // given
        String sentence = "In fact - in all of the Nordics, you’d have a hard time finding a product range as strong and diversified as ours (and we can give you excellent liquidity in local currencies too).";

        // when
        List<String> orderedWords = parser.splitToWordsOrdered(sentence);

        // then
        Assert.assertEquals(33, orderedWords.size());
        Assert.assertEquals("a", orderedWords.get(0));
        Assert.assertEquals("a", orderedWords.get(1));
        Assert.assertEquals("all", orderedWords.get(2));
        Assert.assertEquals("and", orderedWords.get(3));
        Assert.assertEquals("and", orderedWords.get(4));
        Assert.assertEquals("as", orderedWords.get(5));
        Assert.assertEquals("as", orderedWords.get(6));
        Assert.assertEquals("can", orderedWords.get(7));
        Assert.assertEquals("currencies", orderedWords.get(8));
        Assert.assertEquals("diversified", orderedWords.get(9));
        Assert.assertEquals("excellent", orderedWords.get(10));
        Assert.assertEquals("fact", orderedWords.get(11));
        Assert.assertEquals("finding", orderedWords.get(12));
        Assert.assertEquals("give", orderedWords.get(13));
        Assert.assertEquals("hard", orderedWords.get(14));
        Assert.assertEquals("have", orderedWords.get(15));
        Assert.assertEquals("in", orderedWords.get(16));
        Assert.assertEquals("in", orderedWords.get(17));
        Assert.assertEquals("In", orderedWords.get(18));
        Assert.assertEquals("liquidity", orderedWords.get(19));
        Assert.assertEquals("local", orderedWords.get(20));
        Assert.assertEquals("Nordics", orderedWords.get(21));
        Assert.assertEquals("of", orderedWords.get(22));
        Assert.assertEquals("ours", orderedWords.get(23));
        Assert.assertEquals("product", orderedWords.get(24));
        Assert.assertEquals("range", orderedWords.get(25));
        Assert.assertEquals("strong", orderedWords.get(26));
        Assert.assertEquals("the", orderedWords.get(27));
        Assert.assertEquals("time", orderedWords.get(28));
        Assert.assertEquals("too", orderedWords.get(29));
        Assert.assertEquals("we", orderedWords.get(30));
        Assert.assertEquals("you", orderedWords.get(31));
        Assert.assertEquals("you'd", orderedWords.get(32));
    }
}



