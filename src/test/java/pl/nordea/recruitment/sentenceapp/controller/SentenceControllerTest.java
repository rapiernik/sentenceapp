package pl.nordea.recruitment.sentenceapp.controller;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.nordea.recruitment.sentenceapp.SentenceappApplication;

import java.io.FileInputStream;
import java.io.InputStream;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SentenceappApplication.class)
@WebAppConfiguration
public class SentenceControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void shouldRespondSuccessfullyWithXMLFile() throws Exception {
        InputStream inputStream = new FileInputStream("src/test/resources/testFile.in");
        InputStream outputStream = new FileInputStream("src/test/resources/testFile.xml");

        MockMultipartFile file = new MockMultipartFile("file", "testFile.in", "text/plain", IOUtils.toByteArray(inputStream));

        mockMvc.perform(MockMvcRequestBuilders.multipart("/processFile/xml")
                .file(file))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE))
                .andExpect(content().bytes(IOUtils.toByteArray(outputStream)));
    }

    @Test
    public void shouldRespondSuccessfullyWithCSVile() throws Exception {
        InputStream inputStream = new FileInputStream("src/test/resources/testFile.in");
        InputStream outputStream = new FileInputStream("src/test/resources/testFile.csv");

        MockMultipartFile file = new MockMultipartFile("file", "testFile.in", "text/plain", IOUtils.toByteArray(inputStream));

        mockMvc.perform(MockMvcRequestBuilders.multipart("/processFile/csv")
                .file(file))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/csv;charset=UTF-8"))
                .andExpect(content().bytes(IOUtils.toByteArray(outputStream)));
    }
}
