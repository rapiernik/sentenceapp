package pl.nordea.recruitment.sentenceapp.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.nordea.recruitment.sentenceapp.model.Sentences;
import pl.nordea.recruitment.sentenceapp.service.impl.SentenceServiceImpl;
import pl.nordea.recruitment.sentenceapp.utils.TextParser;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SentenceServiceImplTest {

    @Mock
    private TextParser parser;

    @InjectMocks
    private SentenceService sentenceService = new SentenceServiceImpl();

    @Before
    public void init() {
        String sentence1 = "What he  shouted was shocking:  停在那儿, 你这肮脏的掠夺者!";
        String sentence2 = "Why was he directing  his  anger at me?";
        String sentence3 = "We combine local expertise with global strength to provide you with a complete portfolio of financial services and solutions.";
        List<String> sentences = Arrays.asList(new String[]{sentence1, sentence2, sentence3});

        List<String> words1 = Arrays.asList(new String[]{"he", "shocking", "shouted", "was", "What", "你这肮脏的掠夺者", "停在那儿"});
        List<String> words2 = Arrays.asList(new String[]{"anger", "at", "directing", "he", "his", "me", "was", "Why"});
        List<String> words3 = Arrays.asList(new String[]{"a", "and", "combine", "complete", "expertise", "financial", "global", "local", "of", "portfolio", "provide", "services", "solutions", "strength", "to", "We", "with", "with", "you"});

        when(parser.splitToSentences(any())).thenReturn(sentences);
        when(parser.splitToWordsOrdered(sentence1)).thenReturn(words1);
        when(parser.splitToWordsOrdered(sentence2)).thenReturn(words2);
        when(parser.splitToWordsOrdered(sentence3)).thenReturn(words3);
    }

    @Test
    public void shouldProcessFileToSentencesClass() throws IOException {
        byte[] array = new byte[1];
        Sentences sentences = sentenceService.processFile(array);

        Assert.assertEquals(7, sentences.getSentences().get(0).getWords().size());
        Assert.assertEquals("he", sentences.getSentences().get(0).getWords().get(0));
        Assert.assertEquals("shocking", sentences.getSentences().get(0).getWords().get(1));
        Assert.assertEquals("shouted", sentences.getSentences().get(0).getWords().get(2));
        Assert.assertEquals("was", sentences.getSentences().get(0).getWords().get(3));
        Assert.assertEquals("What", sentences.getSentences().get(0).getWords().get(4));
        Assert.assertEquals("你这肮脏的掠夺者", sentences.getSentences().get(0).getWords().get(5));
        Assert.assertEquals("停在那儿", sentences.getSentences().get(0).getWords().get(6));

        Assert.assertEquals(8, sentences.getSentences().get(1).getWords().size());
        Assert.assertEquals("anger", sentences.getSentences().get(1).getWords().get(0));
        Assert.assertEquals("at", sentences.getSentences().get(1).getWords().get(1));
        Assert.assertEquals("directing", sentences.getSentences().get(1).getWords().get(2));
        Assert.assertEquals("he", sentences.getSentences().get(1).getWords().get(3));
        Assert.assertEquals("his", sentences.getSentences().get(1).getWords().get(4));
        Assert.assertEquals("me", sentences.getSentences().get(1).getWords().get(5));
        Assert.assertEquals("was", sentences.getSentences().get(1).getWords().get(6));
        Assert.assertEquals("Why", sentences.getSentences().get(1).getWords().get(7));

        Assert.assertEquals(19, sentences.getSentences().get(2).getWords().size());
        Assert.assertEquals("a", sentences.getSentences().get(2).getWords().get(0));
        Assert.assertEquals("and", sentences.getSentences().get(2).getWords().get(1));
        Assert.assertEquals("combine", sentences.getSentences().get(2).getWords().get(2));
        Assert.assertEquals("complete", sentences.getSentences().get(2).getWords().get(3));
        Assert.assertEquals("expertise", sentences.getSentences().get(2).getWords().get(4));
        Assert.assertEquals("financial", sentences.getSentences().get(2).getWords().get(5));
        Assert.assertEquals("global", sentences.getSentences().get(2).getWords().get(6));
        Assert.assertEquals("local", sentences.getSentences().get(2).getWords().get(7));
        Assert.assertEquals("of", sentences.getSentences().get(2).getWords().get(8));
        Assert.assertEquals("portfolio", sentences.getSentences().get(2).getWords().get(9));
        Assert.assertEquals("provide", sentences.getSentences().get(2).getWords().get(10));
        Assert.assertEquals("services", sentences.getSentences().get(2).getWords().get(11));
        Assert.assertEquals("solutions", sentences.getSentences().get(2).getWords().get(12));
        Assert.assertEquals("strength", sentences.getSentences().get(2).getWords().get(13));
        Assert.assertEquals("to", sentences.getSentences().get(2).getWords().get(14));
        Assert.assertEquals("We", sentences.getSentences().get(2).getWords().get(15));
        Assert.assertEquals("with", sentences.getSentences().get(2).getWords().get(16));
        Assert.assertEquals("with", sentences.getSentences().get(2).getWords().get(17));
        Assert.assertEquals("you", sentences.getSentences().get(2).getWords().get(18));
    }
}
