package pl.nordea.recruitment.sentenceapp.utils.impl;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;
import pl.nordea.recruitment.sentenceapp.exception.SentenceRuntimeException;
import pl.nordea.recruitment.sentenceapp.model.Sentence;
import pl.nordea.recruitment.sentenceapp.model.Sentences;
import pl.nordea.recruitment.sentenceapp.utils.ResponseWriter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;

@Component
public class CommonsCsvResponseWriter implements ResponseWriter {

    private static final String CSV_WRITE_EX_MESSAGE = "Exception during csv writing";

    @Override
    public void writeToFile(PrintWriter writer, Sentences sentences) {

        // generating header for words
        int maxColumnNumber = sentences.getSentences().stream().max(Comparator.comparingInt(o -> o.getWords().size())).get().getWords().size();
        String[] columns = new String[maxColumnNumber + 1];
        columns[0] = "";
        for (int i = 1; i < columns.length; i++) {
            columns[i] = "Word " + i;
        }

        // adding first column for sentences
        int sentenceNumber = 1;
        for (Sentence sentence : sentences.getSentences()) {
            sentence.getWords().add(0, "Sentence " + sentenceNumber);
            sentenceNumber++;
        }

        try {
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.EXCEL.withHeader(columns));
            for (Sentence sentence : sentences.getSentences()) {
                csvPrinter.printRecord(sentence.getWords().stream().toArray(String[]::new));
            }
            csvPrinter.flush();
        } catch (IOException e) {
            throw new SentenceRuntimeException(CSV_WRITE_EX_MESSAGE, e);
        }
    }
}
