package pl.nordea.recruitment.sentenceapp.utils;

import java.util.List;

public interface TextParser {

    List<String> splitToSentences(String inputText);

    List<String> splitToWordsOrdered(String sentence);
}
