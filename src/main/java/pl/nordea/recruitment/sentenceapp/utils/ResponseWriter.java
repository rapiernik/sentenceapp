package pl.nordea.recruitment.sentenceapp.utils;

import pl.nordea.recruitment.sentenceapp.model.Sentences;

import java.io.PrintWriter;

public interface ResponseWriter {

    void writeToFile(PrintWriter writer, Sentences sentences);
}
