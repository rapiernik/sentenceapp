package pl.nordea.recruitment.sentenceapp.utils.impl;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import org.springframework.stereotype.Component;
import pl.nordea.recruitment.sentenceapp.exception.SentenceRuntimeException;
import pl.nordea.recruitment.sentenceapp.utils.TextParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class StanfordAndOpenNLPTextParser implements TextParser {

    private static final String TEXT_PARSE_EX_MESSAGE = "Exception during text parsing";

    private static final String SENTENCE_DETECTOR_FILE_PATH = "/opennlp/en-sent.bin";

    private static final String PUNCTUATION_SYMBOLS_REGEX = "[\\p{Punct}\\s]+";
    private static final String QUESTION_EXCLAMATION_MARKS = "[?!]";

    // split to sentences using OpenNLP
    @Override
    public List<String> splitToSentences(String inputText) {
        InputStream is = getClass().getResourceAsStream(SENTENCE_DETECTOR_FILE_PATH);
        SentenceModel model;
        try {
            model = new SentenceModel(is);
        } catch (IOException e) {
            throw new SentenceRuntimeException(TEXT_PARSE_EX_MESSAGE, e);
        }
        SentenceDetectorME detector = new SentenceDetectorME(model);
        String sentences[] = detector.sentDetect(inputText);

        List<String> outputList = new ArrayList<>();
        for (String sentence : sentences) {
            List<String> additionalSplitSentenceList = splitIfNotSingleSentence(sentence);
            if (additionalSplitSentenceList.size() == 1) {
                outputList.add(sentence);
            } else {
                for (String str : additionalSplitSentenceList) {
                    outputList.add(str);
                }
            }
        }

        return outputList;
    }

    // split to words using StanfordNLP
    @Override
    public List<String> splitToWordsOrdered(String sentence) {
        List<String> labels = new ArrayList<>();
        PTBTokenizer ptbt = new PTBTokenizer<>(new StringReader(sentence),
                new CoreLabelTokenFactory(), "");
        while (ptbt.hasNext()) {
            CoreLabel label = (CoreLabel) ptbt.next();
            labels.add(label.value());
        }

        List<String> output = new ArrayList<>();
        fixParsedData(labels.toArray(new String[0]), output);

        Collections.sort(output, String.CASE_INSENSITIVE_ORDER);

        return getFilteredWords(output);
    }

    private void fixParsedData(String[] tokens, List<String> output) {
        for (int i = tokens.length - 1; i >= 0; i--) {
            if (tokens[i].startsWith("-") && tokens[i].endsWith("-")) {
                tokens[i] = "";
            }

            List<String> fixedTokens = Arrays.asList(tokens);
            if (i > 0 && fixedTokens.get(i).contains("'")) {
                output.add(fixedTokens.get(i - 1) + fixedTokens.get(i));
                i--;
            } else {
                output.add(fixedTokens.get(i));
            }
        }
    }

    private List<String> getFilteredWords(List<String> tokenList) {
        return tokenList.stream().filter(o -> !o.isEmpty()).filter(o -> !o.matches(PUNCTUATION_SYMBOLS_REGEX)).collect(Collectors.toList());
    }

    private List<String> splitIfNotSingleSentence(String sentence) {
        return Arrays.asList(sentence.split(QUESTION_EXCLAMATION_MARKS));
    }
}