package pl.nordea.recruitment.sentenceapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.nordea.recruitment.sentenceapp.model.Sentence;
import pl.nordea.recruitment.sentenceapp.model.Sentences;
import pl.nordea.recruitment.sentenceapp.service.SentenceService;
import pl.nordea.recruitment.sentenceapp.utils.TextParser;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class SentenceServiceImpl implements SentenceService {

    @Autowired
    private TextParser parser;

    @Override
    public Sentences processFile(byte[] data) {
        String textToProcess = new String(data, StandardCharsets.UTF_8);

        List<Sentence> sentenceList = new ArrayList<>();

        // parsing text to sentences
        List<String> sentenceStringList = parser.splitToSentences(textToProcess);

        // parsing sentences to words
        for (String sentence : sentenceStringList) {
            List<String> wordStringList = parser.splitToWordsOrdered(sentence);
            sentenceList.add(new Sentence(wordStringList));
        }

        return new Sentences(sentenceList);
    }
}
