package pl.nordea.recruitment.sentenceapp.service;

import pl.nordea.recruitment.sentenceapp.model.Sentences;

public interface SentenceService {

    Sentences processFile(byte[] data);
}
