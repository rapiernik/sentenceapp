package pl.nordea.recruitment.sentenceapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.nordea.recruitment.sentenceapp.exception.SentenceRuntimeException;
import pl.nordea.recruitment.sentenceapp.model.Sentences;
import pl.nordea.recruitment.sentenceapp.service.SentenceService;
import pl.nordea.recruitment.sentenceapp.utils.ResponseWriter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class SentenceController {

    private static final String INPUT_FILE_EX_MESSAGE = "Request input file exception";
    private static final String RESPONSE_WRITER_EX_MESSAGE = "Response writer exception";

    @Autowired
    private SentenceService sentenceService;

    @Autowired
    private ResponseWriter csvWriter;

    @PostMapping(value = "/processFile/xml", produces = MediaType.APPLICATION_XML_VALUE)
    public Sentences exportToXmlFile(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        String fileName = getOutputFileName(file.getOriginalFilename());

        response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xml");

        Sentences sentences = getSentencesFromInputFile(file);

        return sentences;
    }

    @PostMapping(value = "/processFile/csv")
    public void exportToCsvFile(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        String fileName = getOutputFileName(file.getOriginalFilename());

        response.setContentType("text/csv; charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".csv");

        Sentences sentences = getSentencesFromInputFile(file);

        try {
            csvWriter.writeToFile(response.getWriter(), sentences);
        } catch (IOException e) {
            throw new SentenceRuntimeException(RESPONSE_WRITER_EX_MESSAGE, e);
        }
    }

    private Sentences getSentencesFromInputFile(MultipartFile file) {
        Sentences sentences;
        try {
            sentences = sentenceService.processFile(file.getBytes());
        } catch (IOException e) {
            throw new SentenceRuntimeException(INPUT_FILE_EX_MESSAGE, e);
        }
        return sentences;
    }

    private String getOutputFileName(String originalFilename) {
        return originalFilename.split(".in")[0];
    }
}
