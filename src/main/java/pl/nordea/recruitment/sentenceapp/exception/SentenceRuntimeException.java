package pl.nordea.recruitment.sentenceapp.exception;

public class SentenceRuntimeException extends RuntimeException {

    public SentenceRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
