package pl.nordea.recruitment.sentenceapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SentenceappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SentenceappApplication.class, args);
	}
}
